# Kettle适配华为云服务开发过程


1 开发思路
--------------

### 1.1 学习Kettle的介绍及简单使用
准备工作：上网搜索一些Kettle的学习资料，比如Pentaho官网提供的文档资料以及B站搜索相关学习视频。
</br>这里提供[Pentaho文档](https://help.hitachivantara.com/Documentation/Pentaho/)的学习链接。

### 1.2 源码编译部署
**目的：通过源码编译部署运行Kettle，便于后续构思与开发。**
</br>在GitHub上拉取Kettle源代码，不同版本的Kettle可能需要不同的JDK环境。 
我最初安装的是JDK8，因此我选择了9.0版本的Kettle进行源码编译部署（后续又使用了9.4版本），部署成功后会生成一个`data-integration`文件夹。
</br>![](./images/1.png)

### 1.3 服务对接可行性分析
源码编译部署成功运行Kettle后，进行华为云产品对接的可行性分析，比如该开源项目有哪些功能可以和华为云服务进行结合、华为云服务提供的SDK能否满足需求。
</br>确认可对接的华为云产品后，可到华为云[SDK中心](https://sdkcenter.developer.huaweicloud.com/)查询对应产品的依赖并添加到pom.xml。
> 我的熟悉程度有限，选择参考友商对Kettle底层的实现，找到与友商产品有关的控件后确认源代码中相应的模块，研究实现逻辑实现华为云产品的对接。比如华为云产品OBS对标友商AWS的S3，而Kettle中有S3 CSV Input和S3 File Output两个控件。
> </br>具体实现请参见[Kettle新增的OBS控件](https://gitee.com/HuaweiCloudDeveloper/Huawei-Kettle-plugins/tree/9.0/)。





2 开发过程中遇到的技术问题
--------------

### 2.1 源码编译部署遇到的问题
* 在GitHub上拉取Kettle源代码时，一定要仔细阅读.md文件，注意`环境要求`和替换`settings.xml`文件。
* 编译过程中由于网络问题，部分依赖如若拉取失败，可手动下载添加到Maven缓存目录中（9.0版本可能pom.xml文件中的依赖版本与Pentaho官方仓库提供的版本稍有不同，需要修改）。

### 2.2 Kettle的启动问题
网上参考的源码编译部署版本较落后，在IDEA中debug方式启动`spoon 类`时有大量报错仍未解决，经过网上的参考建议，在IDEA中成功启动的Kettle里也没有对应的plugins模块的插件（插件不全）。
</br>![](./images/2.png)
</br>因此另辟蹊径，通过将plugins模块打包后丢入之前解压的`data-integration`文件夹下的plugins文件夹中，双击`Spoon.bat`启动（显然代码开发时测试会变得繁琐）。
</br>![](./images/3.png)

### 2.3 友商接口与华为云产品接口差异问题
友商的接口字段以及实现类与华为云产品的有差异，需要花时间去看华为云产品的API。
</br>比如在对接华为云产品对象存储服务（Object Storage Service）中，遇到了`OBS桶中对象无法全部列出的问题`。
> **原因：** 在测试账号中，OBS对象有2W+，而OBS的API中listObjects()方法返回的对象的maxKeys字段最大值为1000，即每次最多只能显示1000个对象，将该字段的值调大也没有反应，还是只显示1000条 。
> </br> **解决方法：** 用当前显示的对象条数与maxKeys作为判断条件，并利用另外一个字段nextMarker，循环调用listObjects()方法，循环了20多次后获取了全部对象，该问题成功解决。代码片段如下：
> </br> ![](./images/4.png)

### 2.4 解决ORC格式时遇到的部分jar包与友商耦合性过高问题
在测试账号中，OBS对象中存在orc格式的文件，而我参考友商做的控件是csv格式的，并不支持解析orc格式。
> **解决思路：** 在Kettle中找到了支持解析ORC格式的两个控件，计划让其能够浏览上OBS文件系统。
> </br> **解决过程：** 最初是计划对ORC控件的模块进行修改，然后打包，后面发现ORC格式是Kettle通过OSGI框架捆绑注册的，本地修改了jar包也没有反应，由于对OSGI框架不了解，且学习资料有限，最终放弃了9.0版本，选择9.4版本（需要安装JDK11）。
> </br>选择9.4的原因是9.4版本中的VFS连接虚拟文件系统可以连接上S3，同样我们也能做一个连接OBS的虚拟文件系统。不过在ORC Input控件中，获取字段操作又做了一个验证的动作，部分jar包中耦合了S3，因此又在这些jar包中加入了OBS，然后重新install并替换原有jar包，还加入了hadoop-obs的jar包与安装了hadoop环境，最终成功解决问题。
> </br> **详细过程可参考：** [源码编译部署启动](https://gitee.com/HuaweiCloudDeveloper/Huawei-Kettle-plugins?_from=gitee_search)

### 2.5 网页版Kettle版本过低问题
最初考虑到用户的体验性，希望能够做成网页版供用户使用。但是Kettle本身是桌面版，因此又找到了[网页版webspoon](https://blog.csdn.net/weixin_57250068/article/details/123844086)，通过docker部署后可以在网页访问。后发现制作了网页版Kettle的团队只维护到9.0版本，而9.4与9.0版本不同（尤其是在VFS连接上做了改动），因此放弃了网页版。