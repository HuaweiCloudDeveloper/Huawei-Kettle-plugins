# 一、Kettle适配OBS
Kettle的功能介绍可参考[pentaho 9.4](https://help.hitachivantara.com/Documentation/Pentaho/9.4)，这里仅介绍Kettle做了哪些适配工作。

（一）适配介绍
--------------
* 新增了VFS Connection的连接类型*HuaweiCloud OBS*，具体使用可参考[VFS连接类型-HuaweiCloud OBS](https://gitee.com/HuaweiCloudDeveloper/Huawei-Kettle-plugins/blob/master/obs-vfs/README_zh_CN.md)

（二）Kettle的启动方式
--------------
本文提供了两种Kettle启动方式。
* 源码编译部署启动；
* 安装包启动。
> 若对Kettle源码和适配OBS过程感兴趣的可以参考第一种方式；若想直接使用Kettle工具的可以参考第二种方式。


# 二、源码编译部署启动

（一）pentaho-kettle源码编译
--------------
不做具体介绍，可参考[pentaho-kettle 9.4](https://github.com/pentaho/pentaho-kettle/tree/9.4)。
> **注意：** 不要使用`mvn clean`，直接使用`mvn install -Posgi -DskipTests`进行安装，最终得到一个`pdi-ce-9.4.0.0-SNAPSHOT-osgi.zip`的压缩包，解压到 xxx\ 目录下得到一个`data-integration`文件夹（即Kettle工具包），接下来进行OBS适配工作。

（二）obs-vfs源码编译
--------------

### 1.编译环境
* 安装JDK11；
* Maven3+；
* obs-vfs的[settings.xml](https://gitee.com/HuaweiCloudDeveloper/Huawei-Kettle-plugins/blob/master/obs-vfs/settings.xml)。

### 2.编译过程
* 进入 xxx\obs-vfs\ 目录下打开cmd命令行窗口；
* 输入命令：`mvn clean install -DskipTests`；
* 进入xxx\obs-vfs\target下解压`pentaho-obs-vfs-9.4.0.0-SNAPSHOT.zip`得到pentaho-obs-vfs-plugin文件夹；
* 将`pentaho-obs-vfs-9.4.0.0-SNAPSHOT.jar`复制到pentaho-obs-vfs-plugin文件夹中，再将pentaho-obs-vfs-plugin文件夹复制到 xxx\data-integration\plugins\ 目录下。

（三）driver源码编译
--------------

### 1.编译环境
* 安装JDK11；
* Maven3+；
* driver的[settings.xml](https://gitee.com/HuaweiCloudDeveloper/Huawei-Kettle-plugins/blob/master/driver/settings.xml)；
* 下载[hadoop-huaweicloud](https://repo.huaweicloud.com/repository/maven/huaweicloudsdk/com/huaweicloud/hadoop-huaweicloud/3.1.1-hw-45/)中`hadoop-huaweicloud-3.1.1-hw-45.jar`和`hadoop-huaweicloud-3.1.1-hw-45.pom`文件。

### 2.编译过程
* 进入 xxx\driver\ 目录下打开cmd命令行窗口；
* 输入命令：`mvn clean install -DskipTests`；
* 如果遇到com.huaweicloud:hadoop-huaweicloud找不到的情况，将前面下载的`hadoop-huaweicloud-3.1.1-hw-45.jar`和`hadoop-huaweicloud-3.1.1-hw-45.pom`文件手动复制到本地仓库 xxx\com\huaweicloud\hadoop-huaweicloud\3.1.1-hw-45\ 目录下；
* 再次输入命令：`mvn clean install -DskipTests`；
* 在 xxx\data-integration\ 目录下搜索`pentaho-hadoop-shims-apache-driver-9.4.0.0-SNAPSHOT.jar`的位置，**删掉pentaho-hadoop-shims-apache-driver-9.4.0.0-SNAPSHOT.jar后，将 xxx\driver\target\ 目录下的pentaho-hadoop-shims-apache-driver-9.4.0.0-SNAPSHOT.jar复制过去**。

（四）启动
--------------

### 1.启动环境
* 安装JDK11；
* 安装Hadoop3.0+。

### 2.启动过程
* 进入 xxx\data-integration\ 目录下，双击`Spoon.bat`；
* 如果要用到数据库，需要手动加入Mysql、Oracle等驱动jar包，可置于 xxx\data-integration\lib\ 目录下。


# 三、安装包启动

（一）环境
--------------
* 安装JDK11；
* 安装Hadoop3.0+。

（二）启动
--------------
* 下载[Kettle]()安装包；
* 解压到 xx\ 目录下；
* 进入 xx\data-integration\ 目录下，双击`Spoon.bat`。