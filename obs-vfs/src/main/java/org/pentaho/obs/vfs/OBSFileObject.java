package org.pentaho.obs.vfs;

import org.apache.commons.vfs2.provider.AbstractFileName;
import org.pentaho.obscommon.OBSCommonFileObject;

public class OBSFileObject extends OBSCommonFileObject {
    public OBSFileObject(AbstractFileName name, OBSFileSystem fileSystem) throws Exception {
        super(name, fileSystem);
    }
}
