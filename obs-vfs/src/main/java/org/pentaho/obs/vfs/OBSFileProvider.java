package org.pentaho.obs.vfs;

import org.apache.commons.vfs2.FileName;
import org.apache.commons.vfs2.FileSystem;
import org.apache.commons.vfs2.FileSystemOptions;
import org.pentaho.obscommon.OBSCommonFileProvider;

public class OBSFileProvider extends OBSCommonFileProvider {
    public static final String SCHEME = "obs";

    public OBSFileProvider() {
        this.setFileNameParser(OBSFileNameParser.getInstance());
    }

    protected FileSystem doCreateFileSystem(FileName name, FileSystemOptions fileSystemOptions) {
        return new OBSFileSystem(name, fileSystemOptions);
    }

}
