package org.pentaho.obs.vfs;

import org.apache.commons.vfs2.FileName;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.provider.AbstractFileName;

public class OBSFileName extends AbstractFileName {
    public static final String DELIMITER = "/";
    private String bucketId;
    private String bucketRelativePath;
    private String keys;

    public OBSFileName( String scheme, String bucketId, String path, FileType type, String keys ) {
        this( scheme, bucketId, path, type );
        this.keys = keys;
    }
    public OBSFileName(String scheme, String bucketId, String path, FileType type) {
        super(scheme, path, type);

        this.bucketId = bucketId;

        if (path.length() > 1) {
            this.bucketRelativePath = path.substring(1);
            if (type.equals(FileType.FOLDER)) {
                this.bucketRelativePath = this.bucketRelativePath + DELIMITER;
            }
        } else {
            this.bucketRelativePath = "";
        }
    }

    public String getURI() {
        StringBuilder buffer = new StringBuilder();
        this.appendRootUri(buffer, false);
        buffer.append(this.getPath());
        return buffer.toString();
    }

    public String getBucketId() {
        return bucketId;
    }

    public String getBucketRelativePath() {
        return bucketRelativePath;
    }

    public FileName createName(String absPath, FileType type) {
        return new OBSFileName(this.getScheme(), this.bucketId, absPath, type);
    }

    protected void appendRootUri(StringBuilder buffer, boolean addPassword) {
        buffer.append( getScheme() );
        buffer.append( ":/" );
        if ( keys != null ) {
            buffer.append( '/' ).append( bucketId );
        }
    }
}
