package org.pentaho.huaweicloud.obs;

import org.eclipse.swt.widgets.Composite;
import org.pentaho.di.connections.annotations.Encrypted;
import org.pentaho.di.connections.vfs.VFSConnectionDetails;
import org.pentaho.di.connections.vfs.VFSDetailsComposite;
import org.pentaho.di.core.logging.LogChannel;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.ui.core.PropsUI;
import org.pentaho.metastore.persist.MetaStoreAttribute;
import org.pentaho.metastore.persist.MetaStoreElementType;
import org.pentaho.obs.vfs.OBSFileProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MetaStoreElementType(
        name = "HuaweiCloud OBS Connection",
        description = "Defines the connection details for an HuaweiCloud OBS connection" )
public class OBSDetails implements VFSConnectionDetails {

    private static LogChannelInterface log = new LogChannel( OBSDetails.class.getName() );
    VFSDetailsComposite vfsDetailsComposite;
    VariableSpace space;

    @MetaStoreAttribute private String name;

    @MetaStoreAttribute private String description;

    @MetaStoreAttribute @Encrypted private String accessKey;

    @MetaStoreAttribute @Encrypted private String secretKey;

    @MetaStoreAttribute private String authType;

    @MetaStoreAttribute private String endpoint;

    @MetaStoreAttribute private String connectionType;

    @Override public String getName() {
        return name;
    }

    @Override public void setName( String name ) {
        this.name = name;
    }

    @Override public String getType() {
        return OBSFileProvider.SCHEME;
    }

    @Override public String getDescription() {
        return description;
    }

    @Override
    public void setDescription( String description ) {
        this.description = description;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey( String accessKey ) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey( String secretKey ) {
        this.secretKey = secretKey;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType( String authType ) {
        this.authType = authType;
    }

    public List<String> getRegions() {
        List<String> names = new ArrayList<>();
        String[] endpoints = new String[] { "ap-southeast-4", "af-south-1", "cn-north-4", "cn-north-1", "cn-north-9", "cn-east-2", "cn-east-3", "cn-south-1", "la-north-2", "na-mexico-1", "sa-brazil-1", "la-south-2", "tr-west-1", "cn-southwest-2", "ap-southeast-2", "ap-southeast-3", "ap-southeast-1" };
        for ( String endpoint : endpoints ) {
            names.add(endpoint);
        }
        return names;
    }

    public String getEndpoint() {

        return endpoint;
    }

    public void setEndpoint( String endpoint ) {
        this.endpoint = endpoint;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType( String connectionType ) {
        this.connectionType = connectionType;
    }

    @Override public Map<String, String> getProperties() {
        Map<String, String> props = new HashMap<>();
        props.put( "name", getName() );
        props.put( "description", getDescription() );
        props.put( "accessKey", getAccessKey() );
        props.put( "secretKey", getSecretKey() );
        props.put( "authType", getAuthType() );
        props.put( "endpoint", getEndpoint() );
        props.put( "connectionType", getConnectionType() );

        return props;
    }

    @Override
    public Object openDialog( Object wCompositeWrapper, Object props ) {
        if ( wCompositeWrapper instanceof Composite && props instanceof PropsUI) {
            vfsDetailsComposite = new OBSDetailComposite( (Composite) wCompositeWrapper, this, (PropsUI) props );
            vfsDetailsComposite.open();
            return vfsDetailsComposite;
        }
        return null;
    }

    @Override
    public void closeDialog() {
        if ( vfsDetailsComposite != null ) {
            vfsDetailsComposite.close();
            vfsDetailsComposite = null;
        }
    }

    @Override public VariableSpace getSpace() {
        return space;
    }

    @Override public void setSpace( VariableSpace space ) {
        this.space = space;
    }
}


