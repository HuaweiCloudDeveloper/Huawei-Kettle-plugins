package org.pentaho.obscommon;

import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.provider.AbstractOriginatingFileProvider;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public abstract class OBSCommonFileProvider extends AbstractOriginatingFileProvider {
    public static final UserAuthenticationData.Type[] AUTHENTICATOR_TYPES;
    protected static final Collection<Capability> capabilities;

    public OBSCommonFileProvider() {
    }

    protected abstract FileSystem doCreateFileSystem(FileName var1, FileSystemOptions var2) throws FileSystemException;

    public Collection<Capability> getCapabilities() {
        return capabilities;
    }

    static {
        AUTHENTICATOR_TYPES = new UserAuthenticationData.Type[]{UserAuthenticationData.USERNAME, UserAuthenticationData.PASSWORD};
        // Added Capability.APPEND_CONTENT
        capabilities = Collections.unmodifiableCollection(Arrays.asList(Capability.CREATE, Capability.DELETE, Capability.RENAME, Capability.GET_TYPE, Capability.LIST_CHILDREN, Capability.READ_CONTENT, Capability.URI, Capability.WRITE_CONTENT, Capability.GET_LAST_MODIFIED, Capability.RANDOM_ACCESS_READ, Capability.APPEND_CONTENT));
    }
}
