package org.pentaho.obscommon;

import org.apache.commons.vfs2.FileSystem;
import org.apache.commons.vfs2.FileSystemConfigBuilder;
import org.apache.commons.vfs2.FileSystemOptions;
import org.pentaho.obs.vfs.OBSFileSystem;

public class OBSCommonFileSystemConfigBuilder extends FileSystemConfigBuilder {

    private static final String NAME = "name";
    private static final String ACCESS_KEY = "accessKey";
    private static final String SECRET_KEY = "secretKey";
    private static final String ENDPOINT = "endpoint";
    private static final String DEFAULT_OBS_CONFIG = "defaultOBSConfig";
    private static final String CONNECTION_TYPE = "connectionType";

    private FileSystemOptions fileSystemOptions;

    public OBSCommonFileSystemConfigBuilder( FileSystemOptions fileSystemOptions ) {
        this.fileSystemOptions = fileSystemOptions;
    }

    public FileSystemOptions getFileSystemOptions() {
        return fileSystemOptions;
    }

    public void setFileSystemOptions( FileSystemOptions fileSystemOptions ) {
        this.fileSystemOptions = fileSystemOptions;
    }

    public void setName( String name ) {
        this.setParam( getFileSystemOptions(), NAME, name );
    }

    public String getName() {
        return (String) this.getParam( getFileSystemOptions(), NAME );
    }

    public void setAccessKey( String accessKey ) {
        this.setParam( getFileSystemOptions(), ACCESS_KEY, accessKey );
    }

    public String getAccessKey() {
        return (String) this.getParam( getFileSystemOptions(), ACCESS_KEY );
    }

    public void setSecretKey( String secretKey ) {
        this.setParam( getFileSystemOptions(), SECRET_KEY, secretKey );
    }

    public String getSecretKey() {
        return (String) this.getParam( getFileSystemOptions(), SECRET_KEY );
    }

    public void setEndpoint( String endpoint ) {
        this.setParam( getFileSystemOptions(), ENDPOINT, endpoint );
    }

    public String getEndpoint() {
        return (String) this.getParam( getFileSystemOptions(), ENDPOINT );
    }

    public String getDefaultOBSConfig() {
        return (String) this.getParam( getFileSystemOptions(), DEFAULT_OBS_CONFIG );
    }

    public void setConnectionType( String connectionType ) {
        this.setParam( getFileSystemOptions(), CONNECTION_TYPE, connectionType );
    }

    public String getConnectionType() {
        return (String) this.getParam( getFileSystemOptions(), CONNECTION_TYPE );
    }

    @Override protected Class<? extends FileSystem> getConfigClass() {
        return OBSFileSystem.class;
    }
}

