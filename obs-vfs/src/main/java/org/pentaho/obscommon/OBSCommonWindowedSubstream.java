package org.pentaho.obscommon;

import java.io.ByteArrayInputStream;

public class OBSCommonWindowedSubstream extends ByteArrayInputStream {
    public OBSCommonWindowedSubstream(byte[] buf) {
        super(buf);
    }

    public synchronized long skip(long n) {
        return n;
    }
}
