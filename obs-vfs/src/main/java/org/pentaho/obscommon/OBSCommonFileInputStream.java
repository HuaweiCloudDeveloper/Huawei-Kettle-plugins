package org.pentaho.obscommon;

import com.obs.services.model.ObsObject;
import org.apache.commons.vfs2.util.MonitorInputStream;

import java.io.IOException;
import java.io.InputStream;

public class OBSCommonFileInputStream extends MonitorInputStream {
    private ObsObject obsObject;

    public OBSCommonFileInputStream(InputStream in, ObsObject obsObject) {
        super(in);
        this.obsObject = obsObject;
    }

    protected void onClose() throws IOException {
        super.onClose();
        InputStream is = new ObsObject().getObjectContent();
        if (is != null) {
            is.close();
        }
    }
}
