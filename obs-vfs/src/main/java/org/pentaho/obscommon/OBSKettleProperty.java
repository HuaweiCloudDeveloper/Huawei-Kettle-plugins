package org.pentaho.obscommon;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.i18n.BaseMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Class that handles operations dealing with kettle property file.
 */
public class OBSKettleProperty {
    private static final Class<?> PKG = OBSKettleProperty.class;
    private static final Logger logger = LoggerFactory.getLogger( OBSKettleProperty.class );
    public static final String OBSVFS_PART_SIZE = "obs.vfs.partSize";

    public String getPartSize() {
        return getProperty( OBSVFS_PART_SIZE );
    }

    public String getProperty( String property ) {
        String filename =  Const.getKettlePropertiesFilename();
        Properties properties;
        String partSizeString = "";
        try {
            properties = EnvUtil.readProperties( filename );
            partSizeString = properties.getProperty( property );
        } catch ( KettleException ke ) {
            logger.error( BaseMessages.getString( PKG, "WARN.OBSCommmon.PropertyNotFound",
                    property, filename ) );
        }
        return partSizeString;
    }
}

