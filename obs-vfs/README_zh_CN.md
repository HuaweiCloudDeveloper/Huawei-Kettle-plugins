# VFS连接类型 - HuaweiCloud OBS
新增虚拟文件系统类型：HuaweiCloud OBS。<br/>
您可以通过PDI中的VFS连接连接到HuaweiCloud OBS虚拟文件系统。


在你开始之前
--------------
* 要求已拥有HuaweiCloud OBS桶，并且可以通过AK\SK访问。
* 要求本机已配置JDK11和Hadoop环境。


VFS连接的使用
--------------

### 创建VFS连接
执行以下步骤，在PDI中创建VFS连接：
1. 启动PDI客户端（Spoon），并新建一个转换或者工作。
2. 在新建的转换或者工作中选择`“主对象树”`选项卡，右键单击`“VFS Connections”`，然后点击`“New...”`打开*新建VFS连接对话框*。
3. Connection Name字段，输入唯一描述此连接的名称。名称可以包含空格，但不能包含特殊字段，例如#、$、%。
4. Connection Type字段，在下拉列表中选择*HuaweiCloud OBS*。
5. **_HuaweiCloud OBS Details_**

| 选项                          | 选项说明                                 |
|-----------------------------|--------------------------------------|
| HuaweiCloud Connection Type | 选择默认的HuaweiCloud来启动HuaweiCloud OBS桶。 |
| Authentication Tpye         | 鉴权方式，选择默认的Access Key/Secret Key。     |
| Endpoint                    | Endpoint。                            |
| Access Key                  | Access Key。                          |
| Secret Key                  | Secret Key。                          |

6. （可选）单击`“测试(T)”`验证连接。
7. 单击`“确认(O)”`完成设置。

### 编辑VFS连接
执行以下步骤编辑现有的VFS连接：
1. 右键单击`“VFS Connections”`，然后点击`“Edit...”`打开*编辑VFS连接对话框*。
2. 选择相应的字段进行修改，如果修改了Connection Name字段，点击确认后会打开*重命名或者复制VFS连接对话框*，根据您的选择重命名或者新建VFS连接。

### 删除VFS连接
执行以下步骤删除现有的VFS连接：
1. 右键单击`“VFS Connections”`，然后点击`“Delete...”`打开*删除VFS连接对话框*。
2. 选择yes或者no。

### 使用VFS连接访问文件
* 在PDI客户端中，选择`“文件(F)”`->`”打开“`打开文件或者`“文件(F)”`->`”另存为“`保存文件，文件位置可以选择VFS虚拟文件系统。
* 在支持VFS连接的步骤中选择`”浏览(B)...“`，选择VFS虚拟文件系统中相应的对象。


支持VFS连接的步骤
--------------
* [Avro输入](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Avro_Input)
* [Avro输出](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Avro_Output)
* [CSV文件输入](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/CSV_File_Input)
* [文本文件输出](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Text_File_Output)
* [ORC输入](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/ORC_Input)
* [ORC输出](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/ORC_Input)
* [Parquet输入](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Parquet_Input)
* [Parquet输出](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Parquet_Output)
* 等等


更多VFS连接信息请参见
--------------
[连接虚拟文件系统](https://help.hitachivantara.com/Documentation/Pentaho/9.4/Products/Connecting_to_Virtual_File_Systems)